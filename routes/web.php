<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', [DashboardController::class, 'mainDashboard']);
Route::get('/{id}/{key}/{co}', [DashboardController::class, 'mainDashboard']);
Route::get('/view/{id}', [DashboardController::class, 'viewmainDashboardbook']);
Route::post('/', [DashboardController::class, 'mainDashboardpost']);
Route::post('/skey', [DashboardController::class, 'mainDashboardpostskey']);

//Route::get('/', 'DashboardController@mainDashboard');
