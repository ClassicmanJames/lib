
    <!-- BEGIN: Vendor JS-->
    <script>
        var assetBaseUrl = "{{ asset('') }}";
    </script>
    <script src="{{asset('asset/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('asset/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('asset/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
    <script src="{{asset('asset/js/sb-admin-2.min.js')}}"></script>
    <script src="{{asset('asset/vendor/chart.js/Chart.min.js')}}"></script>
    <script src="{{asset('asset/js/demo/chart-area-demo.js')}}"></script>
    <script src="{{asset('asset/js/demo/chart-pie-demo.js')}}"></script>

    <!-- BEGIN Vendor JS-->


    <!-- BEGIN: Page Vendor JS-->
    @yield('vendor-scripts')

    <!-- BEGIN: Page JS-->
    @yield('page-scripts')
    <!-- END: Page JS-->



