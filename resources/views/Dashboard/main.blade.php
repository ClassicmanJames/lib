@extends('layouts.mainlayout')
@section('content')

    <?php

      ///  echo $id;
    if($id != "")
    {
        $id = $id;
    }
    $curl = curl_init();


    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://apis-3015.lib.cmu.ac.th/exam/book',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
    "keyword":"'.$key.'",
    "category_id": "'.$id.'",
    "skip": 0,
    "limit": '.$co.'
}',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    $re = json_decode($response);
    ?>



        <!-- Sidebar -->

        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">



            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">


                    <!-- Topbar Search -->


                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->


                        <!-- Nav Item - Alerts -->


                        <!-- Nav Item - Messages -->


                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->


                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800"><?php echo $re->count.' '.'record' ?></h1>

                        <form action="{{url('/')}}" method="post" id="stmp">
                            @csrf
                        <input type="hidden" name="key" id="key" value="<?php echo $key?>">
                        <input type="hidden" name="co" id="co" value="<?php echo $co?>">
                        <input type="hidden" id="id" name="id" value="<?php echo $id?>">
                        <select class="" name="cop" id="cop" onchange="myFunction(this.value)">
                            <option value="5">5</option>
                            <option value="15">15</option>
                            <option value="50">50</option>
                        </select>
                        </form>

                        <br/>
                        <form action="{{ url('/skey') }}" method="post" id="stmp">
                            @csrf
                            <input type="text" name="skey" id="skey" value="">
                            <button type="submit">ค้นหา</button>

                        </form>
                    </div>

                    <!-- Content Row -->
                    <div class="row">

                        <?php
                        foreach($re->items as $mydata)
                        {

                            ?>


                        <div class="col-lg-3">
                            <!-- Default Card Example -->

                            <div class="card mb-4">

                                <div class="card-body">
                                    <img src="<?php echo $mydata->volumeInfo->imageLinks->smallThumbnail?>">
                                    <a href="{{ url('/view/'.$mydata->_id ) }} ">
                                    <p><?php echo $mydata->volumeInfo->title?></p>
                                    </a>

                                    <?php

                                    if(isset($mydata->volumeInfo->authors))
                                    {
                                    $a = count($mydata->volumeInfo->authors);
                                   // echo $a;

                                    $addd = '';
                                            foreach ($mydata->volumeInfo->authors as $an)
                                            {
                                                $addd = $addd.' '.$an;
                                            }



                                    ?>
                                    <p><?php echo 'authors'.' '. $addd?></p>
                                    <?php } ?>
                                    <p><?php echo 'subjects'.' '. $mydata->subjects?></p>

                                </div>
                            </div>
                            </a>
                        </div>

                        <?php } ?>









                    </div>




                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->


@endsection

@section('page-scripts')
      <script>
                function myFunction() {
                    $('#stmp').submit();

                }
     </script>



@endsection
